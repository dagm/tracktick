<?php


class ElectronicItem
{
    /**
     * @var float
     */
    public $price;
    /**
     * @var string
     */
    private $type;
    public $wired;
    const ELECTRONIC_ITEM_TELEVISION = 'television';
    const ELECTRONIC_ITEM_CONSOLE = 'console';
    const ELECTRONIC_ITEM_MICROWAVE = 'microwave';
    const ELECTRONIC_ITEM_CONTROLLER = 'controller';
    public static $types = array(
        self::ELECTRONIC_ITEM_CONSOLE,
        self::ELECTRONIC_ITEM_MICROWAVE,
        self::ELECTRONIC_ITEM_TELEVISION,
        SELF::ELECTRONIC_ITEM_CONTROLLER
    );
    function getPrice()
    {
        return $this->price;
    }
    function getType()
    {
        return $this->type;
    }
    function getWired()
    {
        return $this->wired;
    }
    function setPrice($price)
    {
        $this->price = $price;
    }
    function setType($type)
    {
        $this->type = $type;
    }
    function setWired($wired)
    {
        $this->wired = $wired;
    }
}



class ElectronicItems
{
    private $items = array();
    public function __construct(array $items)
    {
        $this->items = $items;
    }
    /**
     * Returns the items depending on the sorting type requested
     *
     * @return array
     */
    public function getSortedItems()
    {
        $sorted = $this->items;
        usort($sorted, function ($item1, $item2) {
            return $item1->getPrice() <=> $item2->getPrice();
        });
        return $sorted;
    }
    /**
     *
     * @param string $type
     * @return array
     */
    public function getItemsByType($type)
    {
        if (in_array($type, ElectronicItem::$types)) {
            $callback = function ($item) use ($type) {
                return $item->getType() == $type;
            };
            return array_filter($this->items, $callback);
        }
        return false;
    }


    /**
     *
     * @return int
     */
    public function getTotal()
    {
        $total = 0;
        foreach ($this->items as $item) {
            $total += $item->getTotal();
        }
        return $total;
    }
}

class CartItem extends ElectronicItem
{
    public $maxExtra;
    public $extras = array();
    public function __construct($price, $maxExtra)
    {
        $this->setPrice($price);
        $this->setMaxExtra($maxExtra);
    }

    /**
     * Sets maxim allowed value for extras 
     * @param int 
     */
    function setMaxExtra($maxExtra)
    {
        $this->maxExtra =  $maxExtra;
    }

    /**
     * Decided if extras are allowed or not 
     * @return bool 
     */
    function maxExtras()
    {
        return sizeof($this->extras) < $this->maxExtra;
    }

    /**
     * adds item to extra if not already full
     * @param ElectronicItem 
     */
    function addExtra($item)
    {
        if ($this->maxExtras()) {
            array_push($this->extras, $item);
        } else {
            echo "Not allowed to add extra";
        }
    }

    /**
     * Calculates the total price including extras 
     * @return int  
     */
    function getTotal()
    {
        $total = $this->getPrice();
        foreach ($this->extras as $item) {
            $total += $item->getTotal();
        }
        return $total;
    }

    /**
     * returns printable string 
     * @return string   
     */
    public function print()
    {
        $printable = "{$this->getType()} {$this->getPrice()}\n";
        foreach ($this->extras as $item) {
            $printable = "{$printable}    + Extra {$item->print()}";
        }
        return "{$printable}\n";
    }
}

class Television extends CartItem
{
    public function __construct($price)
    {
        $this->setPrice($price);
        $this->setType($this::ELECTRONIC_ITEM_TELEVISION);
        $this->setMaxExtra(PHP_INT_MAX);
    }
}


class Console extends CartItem
{
    public function __construct($price)
    {
        $this->setType($this::ELECTRONIC_ITEM_CONSOLE);
        $this->setPrice($price);
        $this->setMaxExtra(4);
    }
}


class Microwave extends CartItem
{
    public function __construct($price)
    {
        $this->setType($this::ELECTRONIC_ITEM_MICROWAVE);
        $this->setPrice($price);
        $this->setMaxExtra(0);
    }
}


class Controller extends CartItem
{
    public function __construct($price, $wired)
    {
        $this->setType($this::ELECTRONIC_ITEM_CONTROLLER);
        $this->setPrice($price);
        $this->setMaxExtra(0);
        $this->setWired($wired);
    }
}
