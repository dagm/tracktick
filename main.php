
<?php

use function PHPSTORM_META\type;

require_once(__DIR__ . '/electronic_items.php');


// Logger utility 
function logger($log, $level)
{
    switch ($level) {
        case $level == "success":
            echo "\033[32m{$log}\033[0m\n";
            break;
        default:
            echo "{$log}\n";
            break;
    }
}

/**
 * 
 * CreateElectronicItem creates a class from a flat object matching the "type" key 
 *  */
function createElectronicItem($item)
{
    switch ($item["type"]) {
        case ElectronicItem::ELECTRONIC_ITEM_CONSOLE:
            return new Console($item["price"]);
        case ElectronicItem::ELECTRONIC_ITEM_TELEVISION:
            return  new Television($item["price"]);
        case ElectronicItem::ELECTRONIC_ITEM_MICROWAVE:
            return new MICROWAVE($item["price"]);
        case ElectronicItem::ELECTRONIC_ITEM_CONTROLLER:
            return  new Controller($item["price"], $item["wired"]);
    }
}

/***
 *  Each test case is a shopping experience. As a result each person gets a cart. 
 *  So from collection items from test case we create electronics item instance. 
 *  Which is called cart.  
 * */
function loadCartItems($items)
{
    $products = array();
    foreach ($items as $item) {
        $product = createElectronicItem($item);
        if (array_key_exists("extras", $item)) {
            foreach ($item["extras"] as $extra) {
                $product->addExtra(createElectronicItem($extra));
            }
        }
        array_push($products, $product);
    }
    return new ElectronicItems($products);
}

/**
 * Runs test cases from a test file 
 * */
function runTestCases()
{
    $testCases = file_get_contents("./test_cases.json");
    $cases = json_decode($testCases, true);
    foreach ($cases as $case) {
        logger("Running case -> {$case["name"]}", "success");
        $cart = loadCartItems($case["data"]);
        $sortedItems = $cart->getSortedItems();
        foreach ($sortedItems as $k => $v) {
            logger("#{$k} {$v->print()}", "default");
        }
        $consoles = new ElectronicItems($cart->getItemsByType("console")); // finding only consoles and getting the total. 
        logger("The total price is {$cart->getTotal()}", "success");
        logger("The cost of the console and it's controllers is {$consoles->getTotal()}", "success");
    }
}

runTestCases();
logger("\nYou can add more test cases on test_cases.json file ", "default");
?>